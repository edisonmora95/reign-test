import { HttpService, Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PostsService } from 'src/posts/posts.service';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(private httpService: HttpService, private postService: PostsService) {}

  async populatePosts() {
    const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

    this.httpService.get(url)
      .subscribe(async (res) => {
        if (res.status === 200) {
          for (const post of res.data.hits) {
            if (!post.title && !post.story_title) {
              continue;
            }
            this.logger.debug(`Post: ${post.title || post.story_title} - from ${post.author}`);
            await this.postService.createIfNotExists({
              title: post.title,
              storyTitle: post.story_title,
              url: post.url,
              storyUrl: post.story_url,
              author: post.author,
              externalId: post.objectID,
              createdAt: post.created_at,
            });
          }
        } else {
          this.logger.error(`Request failed: ${JSON.stringify(res.data)}`);
        }
      })
  }

  @Cron(CronExpression.EVERY_HOUR)
  handleCron() {
    this.populatePosts();
  }
}
