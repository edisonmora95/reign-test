import { forwardRef, HttpModule, Module } from '@nestjs/common';
import { PostsModule } from 'src/posts/posts.module';
import { TasksService } from './tasks.service';

@Module({
  providers: [TasksService],
  imports: [
    HttpModule,
    forwardRef(() => PostsModule),
  ],
  exports: [TasksService],
})
export class TasksModule {}
