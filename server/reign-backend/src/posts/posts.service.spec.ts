import { Test, TestingModule } from '@nestjs/testing';

import { PostsService } from './posts.service';
import { PostRepository } from './post.repository';

class PostRepositoryFake {
  public async create(): Promise<void> {}
  public async findAll(): Promise<void> {}
  public async findOne(): Promise<void> {}
  public async findOneAndUpdate(): Promise<void> {}
  public async count(): Promise<void> {}
}

describe('PostsService', () => {
  describe("Create post", () => {
    let service: PostsService;
    let repository: PostRepository;
    const testPostData = {
      title: "Test",
      storyTitle: "",
      url: "http://",
      storyUrl: "",
      author: "edisonmora95",
      isDeleted: false,
      createdAt: new Date(),
      externalId: "",
    };

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          PostsService,
          {
            provide: PostRepository.name,
            useClass: PostRepositoryFake,
          }
        ],
      }).compile();

      service = module.get<PostsService>(PostsService.name);
      repository = module.get(PostRepository.name);
    });

    it('1) Should use PostRepository.create', async () => {
      const postRepositoryCreateSpy = jest
        .spyOn(repository, "create");

      await service.create(testPostData);
      expect(postRepositoryCreateSpy).toBeCalled();
      expect(postRepositoryCreateSpy).toBeCalledWith(testPostData);
    });

    it('2) Should use PostRepository.create with the same payload', async () => {
      const postRepositoryCreateSpy = jest
        .spyOn(repository, "create");

      await service.create(testPostData);
      expect(postRepositoryCreateSpy).toBeCalledWith(testPostData);
    });

    it('3) Should use PostRepository.create only once', async () => {
      const postRepositoryCreateSpy = jest
        .spyOn(repository, "create");

      await service.create(testPostData);
      expect(postRepositoryCreateSpy).toBeCalledTimes(1);
    });
  });

  describe("Find posts", () => {
    let service: PostsService;
    let repository: PostRepository;
    const testQueryData = {
      isDeleted: false,
      limit: 12,
      skip: 0,
    };

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          PostsService,
          {
            provide: PostRepository.name,
            useClass: PostRepositoryFake,
          }
        ],
      }).compile();

      service = module.get<PostsService>(PostsService.name);
      repository = module.get(PostRepository.name);
    });

    it('1) Should use PostRepository.findAll', async () => {
      const postRepositoryFindAllSpy = jest
        .spyOn(repository, "findAll");

      await service.findAll(testQueryData);
      expect(postRepositoryFindAllSpy).toBeCalled();
    });

    it('2) Should use PostRepository.findAll with', async () => {
      const postRepositoryFindAllSpy = jest
        .spyOn(repository, "findAll");

      await service.findAll(testQueryData);
      const query = { isDeleted: false };
      const options = {
        limit: 12,
        skip: 0,
        lean: true,
        sort: {
          createdAt: -1,
        },
      };
      expect(postRepositoryFindAllSpy).toBeCalledWith(query, {}, options);
    });

    it('3) Should use PostRepository.findAll only once', async () => {
      const postRepositoryFindAllSpy = jest
        .spyOn(repository, "findAll");

      await service.findAll(testQueryData);
      expect(postRepositoryFindAllSpy).toBeCalledTimes(1);
    });
  });

  describe("Delete post", () => {
    let service: PostsService;
    let repository: PostRepository;
    const deletePostData = "123";

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          PostsService,
          {
            provide: PostRepository.name,
            useClass: PostRepositoryFake,
          }
        ],
      }).compile();

      service = module.get<PostsService>(PostsService.name);
      repository = module.get(PostRepository.name);
    });

    it('1) Should use PostRepository.findOneAndUpdate', async () => {
      const postRepositoryDeleteOneSpy = jest
        .spyOn(repository, "findOneAndUpdate");

      await service.deleteOne(deletePostData);
      expect(postRepositoryDeleteOneSpy).toBeCalled();
    });

    it('2) Should use PostRepository.findOneAndUpdate with the correct payload', async () => {
      const postRepositoryDeleteOneSpy = jest
        .spyOn(repository, "findOneAndUpdate");

      await service.deleteOne(deletePostData);
      const query = { _id: deletePostData };
      const update = {
        $set: {
          isDeleted: true,
        },
      };
      const options = {
        lean: true,
        new: true,
      };
      expect(postRepositoryDeleteOneSpy).toBeCalledWith(query, update, options);
    });

    it('3) Should use PostRepository.findOneAndUpdate only once', async () => {
      const postRepositoryDeleteOneSpy = jest
        .spyOn(repository, "findOneAndUpdate");

      await service.deleteOne(deletePostData);
      expect(postRepositoryDeleteOneSpy).toBeCalledTimes(1);
    });
  });

  describe("CreateIfNotExists post", () => {
    let service: PostsService;
    let repository: PostRepository;
    const testPostData = {
      title: "Test",
      storyTitle: "",
      url: "http://",
      storyUrl: "",
      author: "edisonmora95",
      isDeleted: false,
      createdAt: new Date(),
      externalId: "123",
    };

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          PostsService,
          {
            provide: PostRepository.name,
            useClass: PostRepositoryFake,
          }
        ],
      }).compile();

      service = module.get<PostsService>(PostsService.name);
      repository = module.get(PostRepository.name);
    });

    it('1) Should use PostRepository.findOne', async () => {
      const postRepositoryFindOneSpy = jest
        .spyOn(repository, "findOne");

      await service.createIfNotExists(testPostData);
      expect(postRepositoryFindOneSpy).toBeCalled();
      const query = { externalId: "123" };
      const options = { lean: true };
      expect(postRepositoryFindOneSpy).toBeCalledWith(query, {}, options);
    });

    it('2) Should use return found post', async () => {
      const postRepositoryFindOneSpy = jest
        .spyOn(repository, "findOne")
        .mockResolvedValue(testPostData);

      const response = await service.createIfNotExists(testPostData);
      expect(response.externalId).toBe(testPostData.externalId);
    });

    it('3) Should use PostRepository.create', async () => {
      const postRepositoryFindOneSpy = jest
        .spyOn(repository, "findOne")
        .mockResolvedValue(null);

      const postRepositoryCreateSpy = jest
        .spyOn(repository, "create");

      await service.createIfNotExists(testPostData);

      expect(postRepositoryCreateSpy).toBeCalledTimes(1);
    });
  });

  describe("Count posts", () => {
    let service: PostsService;
    let repository: PostRepository;
    const testQueryData = {
      isDeleted: false,
      limit: 12,
      skip: 0,
    };

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          PostsService,
          {
            provide: PostRepository.name,
            useClass: PostRepositoryFake,
          }
        ],
      }).compile();

      service = module.get<PostsService>(PostsService.name);
      repository = module.get(PostRepository.name);
    });

    it('1) Should use PostRepository.count', async () => {
      const postRepositoryCountSpy = jest
        .spyOn(repository, "count");

      await service.count(testQueryData);
      expect(postRepositoryCountSpy).toBeCalled();
    });

    it('2) Should use PostRepository.count with correct arguments', async () => {
      const postRepositoryCountSpy = jest
        .spyOn(repository, "count");

      await service.count(testQueryData);
      const query = {
        isDeleted: false,
      };
      expect(postRepositoryCountSpy).toBeCalledWith(query);
    });

  });
});
