import { Document } from "mongoose";
import { Prop, SchemaFactory, Schema } from "@nestjs/mongoose";

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop()
  title: String;

  @Prop()
  storyTitle: String;

  @Prop()
  url: String;

  @Prop()
  storyUrl: String;

  @Prop({
    required: true,
  })
  createdAt: Date;

  @Prop()
  author: String;

  @Prop({
    required: true,
    index: true,
  })
  externalId: String;

  @Prop({
    default: false,
  })
  isDeleted: Boolean;
}

export const PostSchema = SchemaFactory.createForClass(Post);
