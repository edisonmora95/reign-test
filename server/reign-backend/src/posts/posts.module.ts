import { MongooseModule } from '@nestjs/mongoose';
import { Module, forwardRef } from '@nestjs/common';

import { PostsService } from './posts.service';
import { PostRepository } from './post.repository';
import { PostsController } from './posts.controller';
import { PostSchema, Post } from './db-schemas/post.schema';

import { TasksModule } from 'src/tasks/tasks.module';
@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Post.name,
        schema: PostSchema,
      },
    ]),
    forwardRef(() => TasksModule),
  ],
  controllers: [PostsController],
  providers: [PostsService, PostRepository],
  exports: [PostsService],
})
export class PostsModule {}
