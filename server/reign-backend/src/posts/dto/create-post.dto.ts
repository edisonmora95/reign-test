export class CreatePostDto {
  title: string;
  storyTitle: string;
  url: string;
  storyUrl: string;
  createdAt: Date;
  author: string;
  externalId: string;
}
