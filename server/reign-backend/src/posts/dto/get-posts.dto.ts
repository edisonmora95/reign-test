export class GetPostsDto {
  limit: number;
  skip: number;
  isDeleted: Boolean;
}
