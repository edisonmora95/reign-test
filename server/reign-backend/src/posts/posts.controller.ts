import { Body, Query, Controller, Delete, Get, Param, Post, Res, UsePipes } from '@nestjs/common';

import { PostsService } from './posts.service';

import { JoiValidationPipe } from 'src/validation.pipe';

import { GetPostsDto } from './dto/get-posts.dto';
import { CreatePostDto } from './dto/create-post.dto';

import { CreatePostSchema } from './schemas/create-post.schema';
import { TasksService } from 'src/tasks/tasks.service';

@Controller('posts')
export class PostsController {
  constructor(private postsService: PostsService, private tasksService: TasksService) {}

  @Get()
  async getPosts(@Res() response, @Query() query: GetPostsDto): Promise<any> {
    const posts = await this.postsService.findAll(query);
    const total = await this.postsService.count(query);
    return response.status(200).send({
      message: 'Posts retrieved',
      data: {
        posts,
        total,
      },
    });
  }

  @Post()
  @UsePipes(new JoiValidationPipe(CreatePostSchema))
  async createPost(@Res() response, @Body() createPostDto: CreatePostDto): Promise<any> {
    await this.postsService.create(createPostDto);
    return response.status(201).send({
      message: 'Post created',
      data: createPostDto,
    });
  }

  @Delete(':id')
  async deletePost(@Res() response, @Param() params): Promise<any> {
    const data = await this.postsService.deleteOne(params.id);
    let message = `Post ${params.id} deleted`;
    let status = 200;
    if (!data) {
      message = `Post ${params.id} does not exist`;
      status = 404;
    }
    return response.status(status).send({
      message,
      data,
    });
  }

  @Post('populate')
  async populate(@Res() response): Promise<any> {
    await this.tasksService.populatePosts();
    return response.status(200).send({
      message: 'Posts populated',
      data: true,
    });
  }
}
