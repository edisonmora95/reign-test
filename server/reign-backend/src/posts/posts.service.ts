import { Inject, Injectable } from '@nestjs/common';

import { Post } from './db-schemas/post.schema';
import { GetPostsDto } from './dto/get-posts.dto';
import { CreatePostDto } from './dto/create-post.dto';
import { PostRepository } from './post.repository';

const LIMIT = 12;
const SKIP = 0;

@Injectable()
export class PostsService {

  constructor(
    @Inject('PostRepository') private readonly postRepository: PostRepository
  ) {}

  async create(post: CreatePostDto): Promise<Post> {
    return this.postRepository.create(post);
  }

  async findAll(query: GetPostsDto): Promise<Post[]> {
    const criteria = {
      ...(query.isDeleted != null ? { isDeleted: query.isDeleted } : {}),
    };
    const projection = {};
    const options = {
      lean: true,
      limit: Number(query.limit || LIMIT),
      skip: Number(query.skip || SKIP),
      sort: {
        createdAt: -1,
      }
    };
    return this.postRepository.findAll(criteria, projection, options);
  }

  async createIfNotExists(post: CreatePostDto): Promise<Post> {
    const query = { externalId: post.externalId };
    const options = { lean: true };
    const existingPost = await this.postRepository.findOne(query, {}, options);
    if (existingPost) {
      return existingPost;
    }

    return this.postRepository.create(post);
  }

  async deleteOne(_id: string): Promise<Post> {
    const query = { _id };
    const update = {
      $set: {
        isDeleted: true,
      },
    };
    const options = {
      lean: true,
      new: true,
    };
    return this.postRepository.findOneAndUpdate(query, update, options);
  }

  async count(query: GetPostsDto): Promise<number> {
    const criteria = {
      ...(query.isDeleted != null ? { isDeleted: query.isDeleted } : {}),
    };
    return this.postRepository.count(criteria);
  }
}
