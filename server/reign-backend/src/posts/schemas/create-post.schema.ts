import * as Joi from "joi";

export const CreatePostSchema = Joi.object({
  title: Joi.string().required(),
  url: Joi.string().required(),
});
