export interface Post {
  title: string;
  url: string;
}
