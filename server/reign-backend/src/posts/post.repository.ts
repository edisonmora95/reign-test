import { Model } from "mongoose";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";

import { CreatePostDto } from "./dto/create-post.dto";
import { PostDocument, Post } from "./db-schemas/post.schema";

@Injectable()
export class PostRepository {
  constructor(
    @InjectModel(Post.name) private postModel: Model<PostDocument>
  ) {}

  async create(post: CreatePostDto): Promise<Post> {
    const data = new this.postModel(post);
    return data.save();
  }

  async findAll(query, projection, options): Promise<Post[]> {
    return this.postModel.find(query, projection, options).exec();
  }

  async findOne(query, projection, options): Promise<Post> {
    return this.postModel.findOne(query, projection, options).exec();
  }

  async findOneAndUpdate(query, update, options): Promise<any> {
    return this.postModel.findOneAndUpdate(query, update, options);
  }

  async count(query): Promise<number> {
    return this.postModel.count(query);
  }
}
