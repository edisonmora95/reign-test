# Reign Test

## Installation

If it is the first time, create the network for the docker containers

```bash
docker network create reign-network
```
Then run this command to create the images & containers for each environment

* Development
```bash
docker-compose up -d
```

* Production
```bash
docker-compose -f docker-compose.production.yml up -d
```

## API

* Get all posts

```bash
curl --location --request GET 'http://localhost:3001/posts?isDeleted=false&limit=10'
```

* Populate posts
This is used to populate the database for the first time

```bash
curl --location --request POST 'http://localhost:3001/posts/populate'
```
