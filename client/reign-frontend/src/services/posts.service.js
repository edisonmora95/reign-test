import axios from "axios";

const BASE_URL = process.env.REACT_APP_API_URL || "http://localhost:3001";

/**
 * Return all posts from the server
 * @param {object} query
 * @param {number} query.limit
 * @param {number} query.skip
 * @param {boolean} query.isDeleted
 */
export const getPosts = async (query) => {
  const url = `${BASE_URL}/posts`;
  const response = await axios.get(url, { params: query });
  return response.data;
};

export const deletePost = async (id) => {
  const url = `${BASE_URL}/posts/${id}`;
  const response = await axios.delete(url);
  return response.data;
};
