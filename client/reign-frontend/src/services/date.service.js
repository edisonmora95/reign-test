import dayjs from "dayjs";

/**
 * @param {string} date
 */
export const parseDate = (date) => {
  if (dayjs(date) > dayjs().startOf("day")) {
    return dayjs(date).format("h:mm a");
  }

  if (dayjs(date) > dayjs().subtract(1, "day").startOf("day")) {
    return "Yesterday";
  }

  return dayjs(date).format("MMM D");
}
