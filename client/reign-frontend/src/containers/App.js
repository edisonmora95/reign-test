import { Component } from "react";

import "./App.css";

import Posts from "../components/Posts/Posts";
import Header from "../components/Header/Header";

import { getPosts, deletePost } from "../services/posts.service";
import { parseDate } from "../services/date.service";

class App extends Component {
  state = {
    posts: [],
    limit: 12,
    skip: 0,
    total: 0,
  };

  async componentDidMount() {
    const response = await getPosts({
      limit: this.state.limit,
      skip: this.state.skip,
      isDeleted: false,
    });
    const posts = response.data.posts.map((post) => {
      return {
        ...post,
        date: parseDate(post.createdAt),
      };
    });
    this.setState({
      posts,
      total: response.data.total,
    });
  }

  /**
   * @param {Event} event
   * @param {string} id
   */
  deletePost = async (event, id) => {
    event.stopPropagation();
    await deletePost(id);

    const posts = [...this.state.posts];

    const postIndex = posts.findIndex((x) => x._id === id);
    posts.splice(postIndex, 1);

    this.setState({ posts });
  }

  render() {
    return (
      <div className="App">
        <Header />
        <Posts
          posts={this.state.posts}
          onDelete={this.deletePost}
        />
      </div>
    );
  }
}

export default App;
