import React, { Component } from "react";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import "./Post.css";

class Post extends Component {
  state = {
    hover: false,
  };

  articleClickHandler (prop) {
    const url = prop.storyUrl || prop.url;
    window.open(url, "_blank");
  }

  onHoverHandler() {
    this.setState({
      hover: !this.state.hover,
    });
  }

  render () {
    return (
      <article
        className="Article"
        onClick={() => { this.articleClickHandler(this.props) }}
        onMouseEnter={() => { this.onHoverHandler() } }
        onMouseLeave={() => { this.onHoverHandler() } }
      >
        <p className="title">{ this.props.storyTitle || this.props.title }</p>
        <p className="author">&ensp;- { this.props.author } -&ensp;</p>
        <p className="date">{ this.props.date }</p>
        {
          this.state.hover ?
            <FontAwesomeIcon
              icon={faTrash}
              className="delete"
              onClick={(e) => { this.props.onDelete(e, this.props._id) }}
            />
          : null
        }
      </article>
    )
  }
}

export default Post;
