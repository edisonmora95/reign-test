import React from "react";

import Post from "./Post/Post";

const posts = (props) => {
  return (
    <main>
      {props.posts.map((value) => {
        return (
          <div key={Math.random()}>
            <Post
              {...value}
              onDelete={props.onDelete}
            ></Post>
            <hr></hr>
          </div>
        )
      })}
    </main>
  );
};

export default posts;
