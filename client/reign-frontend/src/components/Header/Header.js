import React from 'react';

import './Header.css';

const header = () => {
  return (
    <header>
      <h1>HN Feed</h1>
      <h3>We {'<'}3 hacker news!</h3>
    </header>
  )
};

export default header;
